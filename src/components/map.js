import React, { Component } from "react";
import Map from "ol/Map";
import View from "ol/View";
import {Vector as SourceVector} from 'ol/source';
import LayerTile from "ol/layer/Tile";
import XYZ from 'ol/source/XYZ';
import LayerVector from "ol/layer/Vector";
import GeoJSON from 'ol/format/GeoJSON';
import Feature from 'ol/Feature';
import Point from "ol/geom/Point";
import {Style, Fill, Stroke, Text} from 'ol/style';
import {defaults} from 'ol/interaction';

import { transform } from 'ol/proj';

import 'font-awesome/css/font-awesome.min.css';

var startLayer = new LayerVector({
    source: new SourceVector({}),
    style:
    new Style({
      text: new Text({
         text: '\uf041',
         font : 'Normal 24px FontAwesome',
         textBaseline: 'bottom',
         fill: new Fill({
           color: "#ff3300",
         })
       })
    })
});

var endLayer = new LayerVector({
    source: new SourceVector({}),
    style:
    new Style({
      text: new Text({
         text: '\uf041',
         font : 'Normal 24px FontAwesome',
         textBaseline: 'bottom',
         fill: new Fill({
           color: "#1E90FF",
         })
       })
    })
});

var routeLayer = new LayerVector({
    source: new SourceVector({}),
});
//source: new SourceOSM()
class MyMap extends Component {
  componentDidMount = () => {
    this.olmap = new Map({
      interactions: defaults({
        doubleClickZoom: false,
      }),
      target: "map",
      controls: [],
      layers: [
        new LayerTile({
          source: new XYZ({
            url: 'http://51.159.59.133:8999/styles/klokantech-basic/{z}/{x}/{y}.png'
          })
        }),
        routeLayer,
        startLayer,
        endLayer
      ],
      view: new View({
        projection: 'EPSG:3857',
        zoom: 6
      })
    });

    this.olmap.getView().setCenter( transform([this.props.center.long, this.props.center.lat], 'EPSG:4326', 'EPSG:3857') );

    this.olmap.on("singleclick", (evt) => {
      var latLong = transform(evt.coordinate, 'EPSG:3857', 'EPSG:4326');
      var lat     = latLong[1];
      var long    = latLong[0];

      this.props.mapClick({lat, long});
    })
  }

  showRoute = () => {
    routeLayer.getSource().clear();

    var { route } = this.props;
    if (!route) {
      return
    }

    const style = new Style({
      fill: new Fill({
          color: '#2E86C1'
      }),
      stroke: new Stroke({
          color: '#2E86C1',
          width: 2
      })
    });

    var features =  (new GeoJSON()).readFeatures(route) ;
    features.forEach( feature => {
      feature.getGeometry().transform('EPSG:4326', 'EPSG:3857');
      feature.setStyle(
        style
      );
      routeLayer.getSource().addFeature( feature );
    } );
  }

  showStart = () => {
    this.showMarker(this.props.start, startLayer)
  }

  showEnd = () => {
    this.showMarker(this.props.end, endLayer)
  }

  showMarker = ( point, layer ) => {
    layer.getSource().clear();
    if(!point) {
      return
    }

      var y = parseFloat(point.lat);
      var x = parseFloat(point.long);
      if ( Number.isNaN(x) ) {
          return;
      }
      var feature = new Feature({
          geometry: new Point( transform([x, y], 'EPSG:4326', 'EPSG:3857' )),
      });
      layer.getSource().addFeature( feature );
  }

  componentDidUpdate(prevProps, prevState) {
    if (prevProps.start !== this.props.start ) {
      this.showStart()
    }
    if (prevProps.end !== this.props.end ) {
      this.showEnd()
    }

    if (prevProps.route !== this.props.route ) {
      this.showRoute()
    }
  }

  render() {
    return (
      <div id="map" style={{ width: "100%", height: "500px" }}>
      </div>
    );
  }
}

export default MyMap;
