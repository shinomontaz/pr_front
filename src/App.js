import React, { Component } from 'react';
import {
  Grid,
  Button,
  Icon
} from 'semantic-ui-react'
import MyMap from './components/map';

import 'react-notifications-component/dist/theme.css'
import 'semantic-ui-css/semantic.min.css'

import './App.css';

const OSRM_URL = process.env.REACT_APP_OSRM_URL;

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      center: {lat:55.751244 ,long: 37.618423},
      start: null,
      end: null,
      route: null,
      error: null,
    };
  }

  handleClick = async (point) => {
    var {start} = this.state;
    if ( !start ) {
      this.setState({
        start: point,
        route: null
      });
    } else {
      this.setState({
        end: point,
        route: null
      });
    }
  }

  componentDidUpdate = (prevProps, prevState) => {
    if ( (!prevState.route || !this.state.route ) && ( this.state.start && this.state.end ) ) {
          var {start, end} = this.state;
          var query = OSRM_URL+'/route/v1/car/'+ start.long +','+ start.lat +';'+ end.long +','+ end.lat + '?geometries=geojson&overview=full';

          console.log(query);

          fetch(query)
          .then(res => res.json())
          .then(
            (result) => {
              this.setState({
                route: result.routes[0].geometry,
                distance: result.routes[0].distance,
                duration: result.routes[0].duration
              });
            },
          ).catch(error => {
            console.error(error);
          });
    }
  }

  handleClear = async () => {
    this.setState({
      start: null,
      end: null,
      route: null
    });
  }

  render() {
    var { center, start, end, route, duration, distance } = this.state;
    return (
      <div className="App">
      <Grid centered>
        <Grid.Column width={2}>
          <Button circular size='mini' basic color='red' onClick={this.handleClear}><Icon name='cancel' />Очистить</Button>
          {route ? (
            <div>
              <div><Icon name='clock outline' />{duration}</div>
              <div><Icon name='arrows alternate horizontal' />{distance}</div>
            </div>
            ) : <div/>
          }
        </Grid.Column>
        <Grid.Column  width={11}>
          <MyMap center={center} start={start} end={end} route={route} mapClick={this.handleClick} />
        </Grid.Column>
      </Grid>
      </div>
    );
  }
}

export default App;
